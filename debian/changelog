python-django-adminsortable (2.0.10-4) UNRELEASED; urgency=low

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Make "Files: *" paragraph the first in the copyright file.
  * Set upstream metadata fields: Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Joost van Baal-Ilić ]
  * d/control: drop myself from Maintainer, set to Debian Python
    Team <team+python@tracker.debian.org>.

  FIXME : please fill in d/control Uploaders field before uploading

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 18 Jul 2020 20:46:16 -0000

python-django-adminsortable (2.0.10-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Team upload.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Fri, 26 Jul 2019 17:51:54 +0200

python-django-adminsortable (2.0.10-2) unstable; urgency=medium

  * Initial public release (Closes: #814532).
  * debian/copyright: completed.
  * debian/control: Standards-Version updated from 3.9.6 to 3.9.7, no other
    changes needed.
  * debian/source/lintian-overrides: added; refer to
    https://github.com/iambrandontaylor/django-admin-sortable/issues/129
    for dealing with minified
    adminsortable/static/adminsortable/js/jquery-ui-django-admin.min.js.
  * debian/control: add Vcs-* tags.
  * debian/control: no longer mention non-existent package
    python-python-django-adminsortable-doc.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 01 Mar 2016 09:51:05 +0100

python-django-adminsortable (2.0.10-1) unstable; urgency=medium

  * Private release for Tilburg University.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 16 Feb 2016 14:46:49 +0000
